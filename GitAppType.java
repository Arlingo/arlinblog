package com.zhikan.agile.pms.integration.enums;

public enum GitAppType {

    IDE("ide");

    GitAppType(String code) {
        this.code = code;
    }

    private final String code;

    public String getCode() {
        return code;
    }
}
