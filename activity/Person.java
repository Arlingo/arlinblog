package activity;

/**
 * @author: Arlin
 * @date: 2024/3/5
 * @description:
 */
public class Person {
    private String name;
    private int age;

    // 构造函数
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    // getter方法
    public String getName() {
        return this.name;
    }

    // setter方法
    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        if (age >= 0) {
            this.age = age;
        } else {
            System.out.println("年龄不能为负数或零！");
        }
    }
}

