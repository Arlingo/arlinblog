package activity;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author: Arlin
 * @date: 2024/3/1
 * @description:
 */
public class SortMapByDate {

    public static void main(String[] args) {
        System.out.println("SortMapByDate");

        Map<String, Integer> unsortedMap = new HashMap<>();
        // 填充数据到unsortedMap
        unsortedMap.put("2024-01-04", 10);
        unsortedMap.put("2023-12-03", 5);
        unsortedMap.put("2024-01-02", 20);

        // 创建一个TreeMap，并提供一个自定义的Comparator来比较字符串类型的日期
        TreeMap<String, Integer> sortedMap = new TreeMap<>(Comparator.naturalOrder());

        // 将所有的条目从未排序的Map复制到已排序的TreeMap
        sortedMap.putAll(unsortedMap);

        // 打印排序后的Map
        for (Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }
}
