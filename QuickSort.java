/**
 * @author: Arlin
 * @description: QuickSort
 * @date: 2024/1/4 10:27
 * @copyright: 2024 智看科技 Inc. All rights reserved.
 */
/**
 * QuickSort class
 */
public class QuickSort {

    /**
     * Main method to test the quickSort function
     * @param args command line arguments
     */
    public static void main(String[] args) {
        int[] arr = {5, 3, 8, 4, 2};
        quickSort(arr, 0, arr.length - 1);
        // 输出排序后的数组
        for (int i : arr) {
            System.out.print(i + " ");
        }
    }

    /**
     * Recursive function to sort the array using quick sort algorithm
     * @param arr the array to be sorted
     * @param low the starting index of the array
     * @param high the ending index of the array
     */
    public static void quickSort(int[] arr, int low, int high) {
        // 如果低索引小于高索引，则进行排序
        if (low < high) {
            // 以最高索引位置的元素作为基准进行划分
            int pivotIndex = partition(arr, low, high);
            // 对划分出来的两部分分别进行递归排序
            quickSort(arr, low, pivotIndex - 1);
            quickSort(arr, pivotIndex + 1, high);
        }
    }

    /**
     * Partition the array based on a pivot element
     * @param arr the array to be partitioned
     * @param low the starting index of the array
     * @param high the ending index of the array
     * @return the index of the pivot element
     */
    public static int partition(int[] arr, int low, int high) {
        // 选取最高索引位置的元素作为基准
        int pivot = arr[high];
        int i = low - 1;
        // 遍历数组，将小于等于基准的元素放到左边，大于基准的元素放到右边
        for (int j = low; j < high; j++) {
            // 如果当前元素小于等于基准，则将i向右移动一位，并交换当前元素和i位置的元素
            if (arr[j] <= pivot) {
                i++;
                swap(arr, i, j);
            }
        }
        // 将基准元素交换到正确的位置上
        swap(arr, i + 1, high);
        // 返回基准元素的最终位置
        return i + 1;
    }

    /**
     * Swap two elements in the array
     * @param arr the array to be swapped
     * @param i the index of the first element
     * @param j the index of the second element
     */
    public static void swap(int[] arr, int i, int j) {
        // 临时变量存储i位置的元素
        int temp = arr[i];
        // 将i位置的元素替换成j位置的元素
        arr[i] = arr[j];
        // 将j位置的元素替换成临时变量的值
        arr[j] = temp;
    }
}

