package code;

import java.util.regex.Pattern;

/**
 * @author: Arlin
 * @description: Test
 * @date: 2024/1/8 16:39
 * @copyright: 2024 智看科技 Inc. All rights reserved.
 */
public class Test {

    private static final Pattern BLANK_LINE_PATTERN = Pattern.compile("^\\s*$");

    private static final Pattern SINGLE_LINE_COMMENT_PATTERN = Pattern.compile("(\\#.*)|(\\#=.*=\\#)");

    public static void main(String[] args) {
        String content = """
                        # 服务器发我发我
                        #=服务范围=#服务微服务
                        # 服务#=服务器=#
                public static void main(String[] args) {
                /*dd*/dd
                ff/*dd*/
                /*dd*/
                            String comment = "// 使用Stream.of方法创建一个包含字符串元素的流";
                            Stream.of("Twix", "Snickers", "Mars")
                // 使用mapMulti方法对每个元素进行映射操作
                                    .mapMulti((item, consumer) -> {
                                        // 调用consumer对象的accept方法，将元素item的大写形式传递给accept方法
                                        consumer.accept(item.toUpperCase());
                                        // 调用consumer对象的accept方法，将元素item的小写形式传递给accept方法
                                        consumer.accept(item.toLowerCase());
                                    })
                                    // 使用forEach方法依次遍历流中的元素，并通过调用System.out::println方法将元素打印到控制台
                                    .forEach(System.out::println);
                                
                                
                            // flatMap 对每个元素进行转换，生成一个包含元素大写和小写的流
                            Stream.of("Twix", "Snickers", "Mars")
                                    .flatMap(item -> Stream.of(item.toUpperCase(), item.toLowerCase()))
                                    .forEach(System.out::println);
                        }
                """;

        String[] lines = content.split("\\r?\\n");
        int blankLines = 0;
        int commentLines = 0;

        for (String line : lines) {
            String code = line.trim();
            if (BLANK_LINE_PATTERN.matcher(code).matches()) {
                blankLines++;
            } else if (SINGLE_LINE_COMMENT_PATTERN.matcher(code).matches()) {
                commentLines++;
            }
        }
        System.out.println("blankLines: " + blankLines);
        System.out.println("commentLines: " + commentLines);
    }
}
