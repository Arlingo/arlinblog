package code;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author: Arlin
 * @description: CodeAnalyzer
 * @date: 2024/1/4 15:21
 * @copyright: 2024 智看科技 Inc. All rights reserved.
 */
public class CodeAnalyzer {

    // 判断文件的程序语言
    public static String getLanguage(String filePath) {
        String extension = filePath.substring(filePath.lastIndexOf('.') + 1);
        switch (extension) {
            case "md":
                return "markdown";
            case "java":
                return "java";
            // 添加其他支持的语言
            default:
                return "";
        }
    }

    private static final Pattern BLANK_LINE_PATTERN = Pattern.compile("^\\s*$");

    private static final Pattern SINGLE_LINE_COMMENT_PATTERN = Pattern.compile("(//.*)|(/\\*.*\\*/)");

    private static final Pattern MULTI_LINE_COMMENT_PATTERN = Pattern.compile("/\\*(?:.|[\\n\\r])*?\\*/");

    private static final Pattern MULTI_LINE_COMMENT_START_PATTERN = Pattern.compile("/\\*.*");

    private static final Pattern MULTI_LINE_COMMENT_END_PATTERN = Pattern.compile(".*\\*/");

    // 统计有效行数
    public static DiffStats countValidLines(String diff, String language) {
        String[] lines = diff.split("\\r?\\n");
        int emptyLines = 0;
        int commentLines = 0;

        if (language.equals("java")) {
            boolean isInMultiLineComment = false;
            for (String line : lines) {
                if (!line.startsWith("+")) {
                    continue;
                }
                String code = line.substring(1).trim();

                Matcher emptyLineMatcher = BLANK_LINE_PATTERN.matcher(code);
                Matcher singleLineCommentMatcher = SINGLE_LINE_COMMENT_PATTERN.matcher(code);
                Matcher multiLineCommentStartMatcher = MULTI_LINE_COMMENT_START_PATTERN.matcher(code);
                Matcher multiLineCommentEndMatcher = MULTI_LINE_COMMENT_END_PATTERN.matcher(code);

                if (emptyLineMatcher.matches()) {
                    emptyLines++;
                } else if (singleLineCommentMatcher.matches()) {
                    commentLines++;
                } else if (isInMultiLineComment) {
                    if (multiLineCommentEndMatcher.matches()) {
                        isInMultiLineComment = false;
                    }
                    commentLines++;
                } else if (multiLineCommentStartMatcher.matches()) {
                    isInMultiLineComment = true;
                    commentLines++;
                }
            }
        }

        int validLines = lines.length - emptyLines - commentLines;
        return new DiffStats(validLines, emptyLines, commentLines);
    }

    public static void main(String[] args) {
        String diff = """
            @@ -0,0 +1,30 @@
            +import java.util.stream.Stream;
            +
            +/**
            + * @author: Arlin
            + * @description: Main
            + * @date: 2023/8/24 10:25
            + * @copyright: 2023 智看科技 Inc. All rights reserved.
            + */
            +public class Main {/**/
            +
            +    public static void main(String[] args) {
            +        // 使用Stream.of方法创建一个包含字符串元素的流，元素包括："Twix", "Snickers", "Mars"
            +        Stream.of("Twix", "Snickers", "Mars")
            +                // 使用mapMulti方法对每个元素进行映射操作
            +                .mapMulti((item, consumer) -> {
            +                    // 调用consumer对象的accept方法，将元素item的大写形式传递给accept方法
            +                    consumer.accept(item.toUpperCase());
            +                    // 调用consumer对象的accept方法，将元素item的小写形式传递给accept方法
            +                    consumer.accept(item.toLowerCase());
            +                })
            +                // 使用forEach方法依次遍历流中的元素，并通过调用System.out::println方法将元素打印到控制台
            +                .forEach(System.out::println);
            +
            +        // flatMap 对每个元素进行转换，生成一个包含元素大写和小写的流
            +        Stream.of("Twix", "Snickers", "Mars")
            +                .flatMap(item -> Stream.of(item.toUpperCase(), item.toLowerCase()))
            +                .forEach(System.out::println);
            +    }
            +
            +}
            """;
//        String filePath = "Main.java";
//        String language = getLanguage(filePath);
//        DiffStats diffStats = countValidLines(diff, language);
//        System.out.println(diffStats);

        String pattern = "^\\+\\s*//.*$|^\\+\\s*/\\*.*\\*/$";
        Pattern regexPattern = Pattern.compile(pattern, Pattern.MULTILINE);
        Matcher matcher = MULTI_LINE_COMMENT_START_PATTERN.matcher(diff);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        System.out.println(count);
    }

    public static class DiffStats {
        private int validCodeLines;
        private int blankLines;
        private int commentLines;

        public DiffStats(int validCodeLines, int blankLines, int commentLines) {
            this.validCodeLines = validCodeLines;
            this.blankLines = blankLines;
            this.commentLines = commentLines;
        }

        public int getValidCodeLines() {
            return validCodeLines;
        }

        public int getBlankLines() {
            return blankLines;
        }

        public int getCommentLines() {
            return commentLines;
        }

        @Override
        public String toString() {
            return "DiffStats{" +
                    "validCodeLines=" + validCodeLines +
                    ", blankLines=" + blankLines +
                    ", commentLines=" + commentLines +
                    '}';
        }
    }

}
