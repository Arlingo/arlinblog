//package examples;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import com.zhikan.agile.pms.automation.util.context.CurrentV2;
//import com.zhikan.agile.pms.common.constants.Constants;
//import com.zhikan.agile.pms.common.enums.BizRespStatusEnum;
//import com.zhikan.agile.pms.common.enums.DeleteFlagEnum;
//import com.zhikan.agile.pms.common.enums.ResourcePermissionEnum;
//import com.zhikan.agile.pms.common.enums.SystemRoleCodeEnum;
//import com.zhikan.agile.pms.common.exception.BizException;
//import com.zhikan.agile.pms.common.util.MapUtil;
//import com.zhikan.agile.pms.common.util.StringUtil;
//import com.zhikan.agile.pms.common.utils.ApiResultUtil;
//import com.zhikan.agile.pms.common.utils.BizAssert;
//import com.zhikan.agile.pms.common.utils.IgnoreExceptionAutoCloseable;
//import com.zhikan.agile.pms.common.utils.MultiTenantUtil;
//import com.zhikan.agile.pms.common.utils.UUIDUtil;
//import com.zhikan.agile.pms.common.utils.UrlUtil;
//import com.zhikan.agile.pms.common.utils.imports.CglibBeanCopyX;
//import com.zhikan.agile.pms.common.utils.imports.Requires;
//import com.zhikan.agile.pms.common.utils.imports.StreamX;
//import com.zhikan.agile.pms.common.vo.AccountSimpleVO;
//import com.zhikan.agile.pms.feature.dto.LabFeatureMsgDTO;
//import com.zhikan.agile.pms.integration.core.config.WorkWechatStoreAppConfig;
//import com.zhikan.agile.pms.integration.core.consts.ReceiveIdType;
//import com.zhikan.agile.pms.integration.core.entity.IntegrationExternalMsg;
//import com.zhikan.agile.pms.integration.core.entity.IntegrationExternalMsgReceiver;
//import com.zhikan.agile.pms.integration.core.entity.IntegrationImApp;
//import com.zhikan.agile.pms.integration.core.entity.IntegrationImOrg;
//import com.zhikan.agile.pms.integration.core.entity.IntegrationWxAuthRecord;
//import com.zhikan.agile.pms.integration.core.entity.helper.IntegrationExternalMsgHelper;
//import com.zhikan.agile.pms.integration.core.entity.helper.IntegrationExternalMsgReceiverHelper;
//import com.zhikan.agile.pms.integration.core.factory.OpenImFactory;
//import com.zhikan.agile.pms.integration.core.manager.IntegrationExternalMsgManager;
//import com.zhikan.agile.pms.integration.core.manager.IntegrationExternalMsgReceiverManager;
//import com.zhikan.agile.pms.integration.core.manager.IntegrationImAppManager;
//import com.zhikan.agile.pms.integration.core.manager.IntegrationImOrgManager;
//import com.zhikan.agile.pms.integration.core.sensor.OpReasonEnum;
//import com.zhikan.agile.pms.integration.core.sensor.ReceivePlatformEnum;
//import com.zhikan.agile.pms.integration.core.sensor.v2.IntegrationSensorHelperV2;
//import com.zhikan.agile.pms.integration.core.service.IntegrationImOrgService;
//import com.zhikan.agile.pms.integration.core.service.IntegrationImService;
//import com.zhikan.agile.pms.integration.core.service.IntegrationStationMessageService;
//import com.zhikan.agile.pms.integration.core.service.IntegrationWxAuthRecordService;
//import com.zhikan.agile.pms.integration.dto.ImAppConfigGetDTO;
//import com.zhikan.agile.pms.integration.dto.ImAppStateDTO;
//import com.zhikan.agile.pms.integration.dto.ImOrgAppAddDTO;
//import com.zhikan.agile.pms.integration.dto.ImOrgAppSelectDTO;
//import com.zhikan.agile.pms.integration.dto.ImOrgDeleteDTO;
//import com.zhikan.agile.pms.integration.dto.StationNotifyDTO;
//import com.zhikan.agile.pms.integration.dto.WorkWechatAuthUrlDTO;
//import com.zhikan.agile.pms.integration.dto.WorkWechatCallbackDTO;
//import com.zhikan.agile.pms.integration.dto.WorkWechatEndpoint;
//import com.zhikan.agile.pms.integration.dto.WorkWechatSendDTO;
//import com.zhikan.agile.pms.integration.dto.WorkWechatSendMarkdownDTO;
//import com.zhikan.agile.pms.integration.dto.WorkWechatStoreAuthUrlDTO;
//import com.zhikan.agile.pms.integration.enums.AppType;
//import com.zhikan.agile.pms.integration.enums.BizCodeIntegrationEnum;
//import com.zhikan.agile.pms.integration.enums.ImType;
//import com.zhikan.agile.pms.integration.enums.IntegrationTypeEnum;
//import com.zhikan.agile.pms.integration.enums.WorkWechatImState;
//import com.zhikan.agile.pms.integration.service.ImWorthService;
//import com.zhikan.agile.pms.integration.service.UnbindAllIntegrationsService;
//import com.zhikan.agile.pms.integration.thirdparty.ImMsgSendResultDTO;
//import com.zhikan.agile.pms.integration.thirdparty.ImNotificationBlankDTO;
//import com.zhikan.agile.pms.integration.thirdparty.NotificationType;
//import com.zhikan.agile.pms.integration.thirdparty.workwechat.CallbackConfigBo;
//import com.zhikan.agile.pms.integration.thirdparty.workwechat.WorkWechatNotificationDTO;
//import com.zhikan.agile.pms.integration.vo.AuthInfoVO;
//import com.zhikan.agile.pms.integration.vo.ImAppConfigVO;
//import com.zhikan.agile.pms.integration.vo.ImAppVO;
//import com.zhikan.agile.pms.integration.vo.ImOrgAppVO;
//import com.zhikan.agile.pms.integration.vo.ImOrgVO;
//import com.zhikan.agile.pms.integration.vo.ImUserVO;
//import com.zhikan.agile.pms.integration.vo.ImUserWorthVO;
//import com.zhikan.agile.pms.integration.vo.SimpleExpireKey;
//import com.zhikan.agile.pms.integration.vo.WorkWechatStoreUserInfoVO;
//import com.zhikan.agile.pms.issue.core.msg.sensor.WorthSensorTracker;
//import com.zhikan.agile.pms.org.core.entity.OrgProductRight;
//import com.zhikan.agile.pms.org.core.entity.Organization;
//import com.zhikan.agile.pms.org.core.manager.OrgProductRightManager;
//import com.zhikan.agile.pms.org.core.manager.OrganizationManager;
//import com.zhikan.agile.pms.org.core.manager.ProductSchemeRuleManager;
//import com.zhikan.agile.pms.org.core.service.AccountService;
//import com.zhikan.agile.pms.org.core.service.PermissionMgrService;
//import com.zhikan.agile.pms.org.dto.product.ProductLineIdDTO;
//import com.zhikan.agile.pms.org.enums.ProductLineEnum;
//import com.zhikan.agile.pms.org.enums.product.RuleCodeEnum;
//import com.zhikan.agile.pms.org.enums.product.range.WorkWechatSupportEnum;
//import com.zhikan.agile.pms.org.vo.product.ProductRuleValueVO;
//import com.zhikan.agile.pms.third.common.aop.annotation.CheckParam;
//import com.zhikan.agile.pms.third.common.enums.FeatureEnum;
//import com.zhikan.agile.pms.third.common.service.FlagsmithService;
//import com.zhikan.agile.pms.third.common.utils.UserContextUtil;
//import com.zhikan.agile.pms.third.id.manager.IdManager;
//import com.zhikan.agile.pms.workwechat.WorkWechatService;
//import com.zhikan.agile.pms.workwechat.core.common.ExternalMsgHelper;
//import com.zhikan.agile.pms.workwechat.core.config.WorkWechatAppConfig;
//import com.zhikan.agile.pms.workwechat.core.constants.StoreCallbackType;
//import com.zhikan.agile.pms.workwechat.core.dto.AccessTokenDTO;
//import com.zhikan.agile.pms.workwechat.core.dto.AuthInfoDTO;
//import com.zhikan.agile.pms.workwechat.core.dto.GetAdminListDTO;
//import com.zhikan.agile.pms.workwechat.core.dto.MarkdownMsgDTO;
//import com.zhikan.agile.pms.workwechat.core.dto.MsgDTO;
//import com.zhikan.agile.pms.workwechat.core.dto.TextCardMsgDTO;
//import com.zhikan.agile.pms.workwechat.core.event.AbstractWorkWechatBaseEvent;
//import com.zhikan.agile.pms.workwechat.core.event.BatchJobResultEvent;
//import com.zhikan.agile.pms.workwechat.core.event.EventTypeEnum;
//import com.zhikan.agile.pms.workwechat.core.event.WorkWechatCommandBaseEvent;
//import com.zhikan.agile.pms.workwechat.core.handler.WorkWechatEventHandler;
//import com.zhikan.agile.pms.workwechat.core.handler.command.WorkWechatCommandEventHandler;
//import com.zhikan.agile.pms.workwechat.core.handler.store.BatchJobResultStoreEventHandler;
//import com.zhikan.agile.pms.workwechat.core.handler.store.WorkWechatStoreEventHandler;
//import com.zhikan.agile.pms.workwechat.core.msg.NotificationSender;
//import com.zhikan.agile.pms.workwechat.core.msg.SpecialMsgProducer;
//import com.zhikan.agile.pms.workwechat.core.sao.AccessTokenManager;
//import com.zhikan.agile.pms.workwechat.core.sao.CorpAccessTokenManager;
//import com.zhikan.agile.pms.workwechat.core.sao.ExportTaskManager;
//import com.zhikan.agile.pms.workwechat.core.sao.ExportTaskStoreManager;
//import com.zhikan.agile.pms.workwechat.core.sao.ProviderAccessTokenManager;
//import com.zhikan.agile.pms.workwechat.core.sao.SuiteAccessTokenManager;
//import com.zhikan.agile.pms.workwechat.core.sao.WorkWechatSAO;
//import com.zhikan.agile.pms.workwechat.core.utils.WorkWechatEncryptUtils;
//import com.zhikan.agile.pms.workwechat.core.vo.AccessTokenVO;
//import com.zhikan.agile.pms.workwechat.core.vo.AdminListVO;
//import com.zhikan.agile.pms.workwechat.core.vo.JsTicketVO;
//import com.zhikan.agile.pms.workwechat.core.vo.MsgVO;
//import com.zhikan.agile.pms.workwechat.core.vo.OpenCorpIdVO;
//import com.zhikan.agile.pms.workwechat.core.vo.UserIdVO;
//import com.zhikan.agile.pms.workwechat.core.vo.UserInfoThirdVO;
//import com.zhikan.agile.pms.worth.vo.WorthMsgVO;
//import com.zhikan.base.api.ApiResult;
//import com.zhikan.base.utils.CglibBeanCopyUtil;
//import com.zhikan.redis.service.StringRedisService;
//import com.zhikan.tenancy.common.DefaultTenantInfo;
//import com.zhikan.tenancy.common.TenantInfoHolder;
//import lombok.NonNull;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.collections4.CollectionUtils;
//import org.apache.commons.collections4.MapUtils;
//import org.apache.commons.lang3.ObjectUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//import tk.mybatis.mapper.entity.Example;
//import tk.mybatis.mapper.weekend.WeekendSqls;
//
//import java.net.*;
//import java.nio.charset.StandardCharsets;
//import java.util.Arrays;
//import java.util.Collection;
//import java.util.Comparator;
//import java.util.Date;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Optional;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.stream.Collectors;
//
///**
// * @author Ferry
// * @title: WorkWechatServiceImpl
// * @projectName agile-pms
// * @description: 企业微信服务
// * @date 2021/11/117:43
// */
//@Service
//@Slf4j
//public class WorkWechatServiceImpl implements WorkWechatService, UnbindAllIntegrationsService {
//
//    @Autowired
//    private List<WorkWechatEventHandler<? extends AbstractWorkWechatBaseEvent, ?>> workWechatEventHandlerList;
//    @Autowired
//    private List<WorkWechatStoreEventHandler<? extends AbstractWorkWechatBaseEvent, ?>> workWechatStoreEventHandlerList;
//    @Autowired
//    private StringRedisService stringRedisService;
//    @Autowired
//    private WorkWechatAppConfig workWechatAppConfig;
//    @Autowired
//    private WorkWechatStoreAppConfig workWechatStoreAppConfig;
//    @Autowired
//    private OrganizationManager organizationManager;
//    @Autowired
//    private IntegrationImOrgService integrationImOrgService;
//    @Autowired
//    private NotificationSender notificationSender;
//    @Autowired
//    private IntegrationExternalMsgManager integrationExternalMsgManager;
//    @Autowired
//    private IntegrationExternalMsgReceiverManager integrationExternalMsgReceiverManager;
//    @Autowired
//    private IdManager idManager;
//    @Autowired
//    private IntegrationImOrgManager integrationImOrgManager;
//    @Autowired
//    private IntegrationImAppManager integrationImAppManager;
//    @Autowired
//    private SpecialMsgProducer specialMsgProducer;
//    @Autowired
//    private WorthSensorTracker worthSensorTracker;
//    @Autowired
//    private IntegrationWxAuthRecordService integrationWxAuthRecordService;
//    @Autowired
//    private OrgProductRightManager orgProductRightManager;
//    @Autowired
//    private ProductSchemeRuleManager productSchemeRuleManager;
//    @Autowired
//    private AccountService accountService;
//    @Autowired
//    private PermissionMgrService permissionMgrService;
//    @Autowired
//    private ImWorthService imWorthService;
//    @Autowired
//    private IntegrationStationMessageService integrationStationMessageService;
//    @Autowired
//    private BatchJobResultStoreEventHandler batchJobResultStoreEventHandler;
//    @Autowired
//    private IntegrationSensorHelperV2 integrationSensorHelperV2;
//    @Autowired
//    private FlagsmithService flagsmithService;
//    @Autowired
//    private ExternalMsgHelper externalMsgHelper;
//
//    @Autowired
//    private IntegrationImService integrationImService;
//
//    private ExecutorService executorService = Executors.newFixedThreadPool(3);
//
//    /**
//     * auth 2.0回调的state参数校验缓存
//     */
//    private static final String AUTH_STATE_KEY_PREF = "pms:workwechat:auth:state";
//
//    private static final Integer EXPIRE = 1800;
//
//    @Override
//    public ApiResult<String> workWechatVerifyCallbackUrl(@NonNull String orgId, WorkWechatCallbackDTO workWechatCallbackDTO) {
//        log.info("work wechat verify url, corpId = {}, workWechatCallbackDTO = {}", orgId, workWechatCallbackDTO);
//        try {
//            ImOrgAppVO imOrgAppVO = getImOrgApp(orgId);
//            String result = WorkWechatEncryptUtils.verifyUrl(workWechatCallbackDTO, integrationImOrgService.imOrgAppVO2CallbackConfigBo(imOrgAppVO));
//            if (StringUtils.isNotBlank(result)){
//                //触发欢迎消息任务
//                OpenImFactory.Type.getTypeByCode(ImType.WORK_WECHAT.getCode()).getOpenImFactory().sendWelcomeMsg();
//                // 发送集成通知
//                integrationStationMessageService.sendEnableNotice(
//                        StationNotifyDTO.builder()
//                                .type(IntegrationTypeEnum.WORK_WECHAT)
//                                .objectId(imOrgAppVO.getImOrgId())
//                                .notifyAccountIdSet(new HashSet<>())
//                                .checkDuplicate(true)
//                                .build());
//            }
//            return ApiResult.success(result);
//        } catch (Exception e){
//            log.warn("work wechat verify callback url failed", e);
//            return ApiResult.error(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//        } finally {
//            TenantInfoHolder.clear();
//        }
//    }
//
//    @Override
//    public ApiResult<Object> callbackEventProcess(@NonNull String orgId, WorkWechatCallbackDTO workWechatCallbackDTO) {
//
//        log.info("work wechat callback event process, orgId = {}, callbackEvent = {}", orgId, workWechatCallbackDTO);
//        try {
//            ImOrgAppVO imOrgAppVO = getImOrgApp(orgId);
//            imOrgAppVO.getImOrg().setOrgUuid(orgId);
//            CallbackConfigBo callbackConfigBo = integrationImOrgService.imOrgAppVO2CallbackConfigBo(imOrgAppVO);
//            String msg = WorkWechatEncryptUtils.decryptMsg(workWechatCallbackDTO, callbackConfigBo);
//            log.debug("work wechat callback msg decrypted , decryptMsg = {}", msg);
//            //TODO 企业微信 处理回调消息 考虑不需要回复的消息转异步的问题
//            for (WorkWechatEventHandler<? extends AbstractWorkWechatBaseEvent, ?> handler : workWechatEventHandlerList){
//                Optional<?> optional = handler.accept(msg, callbackConfigBo, imOrgAppVO);
//                if (optional.isPresent()) {
//                    return ApiResult.success(optional.get());
//                }
//            }
//        } catch (Throwable e){
//            log.warn("decrypt msg error, orgId = " + orgId + ", requestData = " + workWechatCallbackDTO.toString(), e);
//            return ApiResult.error(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//        } finally {
//            TenantInfoHolder.clear();
//        }
//        return ApiResult.error(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//    }
//
//    @Override
//    public ApiResult<String> workWechatVerifyCallbackUrlV2(@NonNull String orgId, WorkWechatCallbackDTO workWechatCallbackDTO) {
//        log.info("work wechat verify url v2, corpId = {}, workWechatCallbackDTO = {}", orgId, workWechatCallbackDTO);
//        try {
//            ImOrgAppVO imOrgAppVO = new ImOrgAppVO();
//            if (Constants.STR_ZERO.equals(orgId)) {
//                // 模板
//                imOrgAppVO.setImApp(ImAppVO.builder()
//                        .callbackToken(workWechatAppConfig.getCallbackToken())
//                        .callbackKey(workWechatAppConfig.getCallbackKey()).build());
//                imOrgAppVO.setImOrg(new ImOrgVO(workWechatAppConfig.getProviderCorpId()));
//            } else {
//                imOrgAppVO = getImOrgApp(orgId);
//                setEncryptExternalOrgId(imOrgAppVO);
//            }
//            String result = WorkWechatEncryptUtils.verifyUrl(workWechatCallbackDTO, integrationImOrgService.imOrgAppVO2CallbackConfigBo(imOrgAppVO));
//            return ApiResult.success(result);
//        } catch (Exception e){
//            log.warn("work wechat verify callback url v2 failed", e);
//            return ApiResult.error(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//        } finally {
//            TenantInfoHolder.clear();
//        }
//    }
//
//    @Override
//    public ApiResult<Object> callbackEventProcessV2(@NonNull String orgId, WorkWechatCallbackDTO workWechatCallbackDTO) {
//        log.info("work wechat callback event process v2, orgId = {}, callbackEvent = {}", orgId, workWechatCallbackDTO);
//        try {
//            ImOrgAppVO imOrgAppVO = new ImOrgAppVO();
//            if (Constants.STR_ZERO.equals(orgId)) {
//                // 模板回调
//                imOrgAppVO.setImApp(ImAppVO.builder()
//                        .callbackToken(workWechatAppConfig.getCallbackToken())
//                        .callbackKey(workWechatAppConfig.getCallbackKey()).build());
//                imOrgAppVO.setImOrg(new ImOrgVO(workWechatAppConfig.getSuiteId()));
//            } else {
//                imOrgAppVO = getImOrgApp(orgId);
//                imOrgAppVO.getImOrg().setOrgUuid(orgId);
//                setEncryptExternalOrgId(imOrgAppVO);
//            }
//            CallbackConfigBo callbackConfigBo = integrationImOrgService.imOrgAppVO2CallbackConfigBo(imOrgAppVO);
//            String msg = WorkWechatEncryptUtils.decryptMsg(workWechatCallbackDTO, callbackConfigBo);
//            log.info("work wechat callback msg decrypted v2, decryptMsg = {}", msg);
//
//            WorkWechatCommandEventHandler<? extends WorkWechatCommandBaseEvent> agentHandler = WorkWechatCommandEventHandler.EventEnum.getHandle(msg);
//            if (agentHandler != null) {
//                Optional<Boolean> optional = agentHandler.accept(msg, AppType.AGENT);
//                if (optional.isPresent() && optional.get()) {
//                    return ApiResult.success(true);
//                }
//                return ApiResult.error(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//            }
//            for (WorkWechatEventHandler<? extends AbstractWorkWechatBaseEvent, ?> handler : workWechatEventHandlerList){
//                Optional<?> optional = handler.accept(msg, callbackConfigBo, imOrgAppVO);
//                if (optional.isPresent()) {
//                    return ApiResult.success(optional.get());
//                }
//            }
//        } catch (Throwable e){
//            log.warn("decrypt msg error v2, orgId = " + orgId + ", requestData = " + workWechatCallbackDTO.toString(), e);
//            throw new BizException(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//        } finally {
//            TenantInfoHolder.clear();
//        }
//        return ApiResult.error(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//    }
//
//    @Override
//    public ApiResult<String> workWechatStoreVerifyCallback(String type, WorkWechatCallbackDTO workWechatCallbackDTO) {
//        log.info("workWechatStoreVerifyCallback, type = {}, workWechatCallbackDTO = {}", type, workWechatCallbackDTO);
//        try {
//            ImOrgAppVO imOrgAppVO = new ImOrgAppVO();
//            imOrgAppVO.setImApp(ImAppVO.builder()
//                    .callbackToken(workWechatStoreAppConfig.getVerificationToken())
//                    .callbackKey(workWechatStoreAppConfig.getEncryptKey()).build());
//            imOrgAppVO.setImOrg(new ImOrgVO(workWechatAppConfig.getProviderCorpId()));
//            String result = WorkWechatEncryptUtils.verifyUrl(workWechatCallbackDTO, integrationImOrgService.imOrgAppVO2CallbackConfigBo(imOrgAppVO));
//            return ApiResult.success(result);
//        } catch (Exception e){
//            log.warn("generate store auth2.0 url failed", e);
//            return ApiResult.error(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//        }
//    }
//
//    @Override
//    public ApiResult<Object> storeCallbackEventProcess(String type, String corpId, WorkWechatCallbackDTO workWechatCallbackDTO) {
//        log.info("storeCallbackEventProcess, type = {}, corpId = {}, callbackEvent = {}", type, corpId, workWechatCallbackDTO);
//        try {
//            ImOrgAppVO imOrgAppVO = new ImOrgAppVO();
//            imOrgAppVO.setImApp(ImAppVO.builder()
//                    .callbackToken(workWechatStoreAppConfig.getVerificationToken())
//                    .callbackKey(workWechatStoreAppConfig.getEncryptKey()).build());
//
//            if (StoreCallbackType.COMMAND.getType().equals(type)) {
//                // 指令回调，receiveid为应用的suiteid
//                imOrgAppVO.setImOrg(new ImOrgVO(workWechatStoreAppConfig.getAppId()));
//                CallbackConfigBo callbackConfigBo = integrationImOrgService.imOrgAppVO2CallbackConfigBo(imOrgAppVO);
//                String msg = WorkWechatEncryptUtils.decryptMsg(workWechatCallbackDTO, callbackConfigBo);
//                log.info("storeCallbackEventProcess, work wechat command callback msg decrypted, decryptMsg = {}", msg);
//
//                WorkWechatCommandEventHandler<? extends WorkWechatCommandBaseEvent> agentHandler = WorkWechatCommandEventHandler.EventEnum.getHandle(msg);
//                if (agentHandler != null) {
//                    Optional<Boolean> optional = agentHandler.accept(msg, AppType.STORE);
//                    if (optional.isPresent() && optional.get()) {
//                        return ApiResult.success(true);
//                    }
//                    return ApiResult.error(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//                }
//            } else if (StoreCallbackType.DATA.getType().equals(type)) {
//                // 数据回调，receiveid为授权企业的corpid
//                BizAssert.isNull(corpId, "data callback corpId is null");
//                imOrgAppVO.setImOrg(new ImOrgVO(corpId));
//                CallbackConfigBo callbackConfigBo = integrationImOrgService.imOrgAppVO2CallbackConfigBo(imOrgAppVO);
//                String msg = WorkWechatEncryptUtils.decryptMsg(workWechatCallbackDTO, callbackConfigBo);
//                log.info("storeCallbackEventProcess, work wechat data callback msg decrypted, decryptMsg = {}", msg);
//
//                for (WorkWechatStoreEventHandler<? extends AbstractWorkWechatBaseEvent, ?> handler : workWechatStoreEventHandlerList){
//                    Optional<?> optional = handler.accept(msg);
//                    if (optional.isPresent()) {
//                        return ApiResult.success(optional.get());
//                    }
//                }
//            }
//        } catch (Throwable e){
//            log.warn("storeCallbackEventProcess, decrypt msg error, requestData = " + workWechatCallbackDTO.toString(), e);
//            throw new BizException(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//        } finally {
//            TenantInfoHolder.clear();
//        }
//        return ApiResult.error(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//    }
//
//    /**
//     * 设置加密的corpId
//     * @param imOrgAppVO
//     */
//    private void setEncryptExternalOrgId(ImOrgAppVO imOrgAppVO) {
//        String encryptOpenId = imOrgAppVO.getImOrg().getEncryptExternalOrgId();
//        if (StringUtils.isBlank(encryptOpenId)) {
//            encryptOpenId = getEncryptOpenId(imOrgAppVO.getImOrg().getExternalOrgId()).getResult();
//            // 保存加密的corpId
//            try (IgnoreExceptionAutoCloseable ignored = MultiTenantUtil.setSkip()) {
//                IntegrationImOrg imOrg = new IntegrationImOrg();
//                imOrg.setId(imOrgAppVO.getImOrgId());
//                imOrg.setEncryptExternalOrgId(encryptOpenId);
//                imOrg.setUpdateBy(Constants.SYS_USER_ID);
//                imOrg.setUpdateTime(new Date());
//                integrationImOrgManager.updateByPrimaryKeySelective(imOrg);
//                integrationImOrgManager.removeImOrgCache(imOrgAppVO.getTenantId(), imOrgAppVO.getImOrg().getExternalOrgId(),
//                        ImType.WORK_WECHAT.getCode(), imOrgAppVO.getImApp().getAppType());
//            }
//        }
//        imOrgAppVO.getImOrg().setExternalOrgId(encryptOpenId);
//    }
//
//    /**
//     * 获取加密后的corpId
//     * @param corpId 企业微信corpId
//     * @return 加密后的corpId
//     */
//    @Override
//    public ApiResult<String> getEncryptOpenId(String corpId) {
//        try {
//            Optional<OpenCorpIdVO> openCorpIdVO;
//            String providerToken =
//                    ProviderAccessTokenManager.getToken(workWechatAppConfig.getProviderCorpId(), workWechatAppConfig.getProviderSecret());
//            openCorpIdVO = WorkWechatSAO.encryptCorpId(providerToken, corpId);
//            return openCorpIdVO.map(corpIdVO -> ApiResult.success(corpIdVO.getOpen_corpid()))
//                    .orElseGet(() -> ApiResult.error(BizCodeIntegrationEnum.IM_APP_UNAUTHORIZED));
//        } catch (Exception e) {
//            log.error("get encrypt open id error, corpId = " + corpId, e);
//            return ApiResult.error(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//        }
//    }
//
//    @Override
//    public ApiResult<String> getWebAuthUrl(WorkWechatAuthUrlDTO workWechatAuthUrlDTO) {
//
//        log.info("generate auth2.0 url, workWechatAuthURLDTO = {}", workWechatAuthUrlDTO);
//        try {
//            String uuidState = UUIDUtil.randomUUID();
//            ImOrgAppVO imOrgAppVO = getImOrgApp(workWechatAuthUrlDTO.getOrgId());
//            String corpId = imOrgAppVO.getImOrg().getExternalOrgId();
//            String authUrl = WorkWechatSAO.getWebAuthUrl(corpId, getRedirectUrl(workWechatAuthUrlDTO), uuidState);
//            cacheState(uuidState);
//            log.debug("generate auth2.0 url, url = {}", authUrl);
//            return ApiResult.success(authUrl);
//        } catch (Exception e) {
//            log.error("generate auth2.0 url failed", e);
//            return ApiResult.error(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//        } finally {
//            TenantInfoHolder.clear();
//        }
//    }
//
//    @Override
//    public ApiResult<String> authCallbackProcess(@NonNull String orgId, String code, String state, String originalPath) {
//
//        try {
//            log.info("auth2.0 callback process, orgId = {}, code = {}, state = {}", orgId, code, state);
//            if (!checkState(state)){
//                log.warn("check auth callback state error , state : {}", state);
//                return ApiResult.error(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//            }
//            removeState(state);
//            ImOrgAppVO imOrgAppVO = getImOrgApp(orgId);
//            Optional<UserIdVO> userIdOp = WorkWechatSAO.getUserInfo(AccessTokenManager.getTokenByType(imOrgAppVO), code);
//            if (!userIdOp.isPresent()){
//                log.warn("get userId failed , state : {}", state);
//                return ApiResult.error(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//            }
//            String url = UrlUtil.getUrlWithParams(URLDecoder.decode(originalPath, StandardCharsets.UTF_8.name()),
//                    Map.of("orgId", orgId, "userId", userIdOp.get().getUserId()));
//            log.debug("auth2.0 callback process, redirectUrl = {}", url);
//            return ApiResult.success(url);
//        } catch (Exception e){
//            log.error("auth callback process failed", e);
//            return ApiResult.error(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//        } finally {
//            TenantInfoHolder.clear();
//        }
//    }
//
//    @Override
//    public ApiResult<String> getStoreWebAuthUrl(WorkWechatStoreAuthUrlDTO workWechatStoreAuthUrlDTO) {
//        log.info("generate store auth2.0 url, workWechatStoreAuthUrlDTO = {}", workWechatStoreAuthUrlDTO);
//        try {
//            String uuidState = UUIDUtil.randomUUID();
//            // 拼接重定向地址
//            String redirectUrl = UrlUtil.getUrlWithParams(workWechatStoreAppConfig.getAuthRedirectUrl(),
//                    Map.of("originalPath", workWechatStoreAuthUrlDTO.getOriginalPath()));
//            String authUrl = WorkWechatSAO.getWebAuthUrl(workWechatStoreAppConfig.getAppId(), redirectUrl, uuidState);
//            cacheState(uuidState);
//            log.debug("generate store auth2.0 url, url = {}", authUrl);
//            return ApiResult.success(authUrl);
//        } catch (Exception e) {
//            log.error("generate store auth2.0 url failed", e);
//            return ApiResult.error(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//        } finally {
//            TenantInfoHolder.clear();
//        }
//    }
//
//    @Override
//    public ApiResult<String> webStoreAuthCallback(String code, String state, String originalPath) {
//        try {
//            log.info("store auth2.0 callback process, code = {}, state = {}", code, state);
//            if (!checkState(state)){
//                log.warn("check store auth callback state error , state : {}", state);
//                return ApiResult.error(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//            }
//            removeState(state);
//            String url = UrlUtil.getUrlWithParams(URLDecoder.decode(originalPath, StandardCharsets.UTF_8), Map.of("code", code));
//            log.debug("store auth2.0 callback process, redirectUrl = {}", url);
//            return ApiResult.success(url);
//        } catch (Exception e){
//            log.error("store auth callback process failed", e);
//            return ApiResult.error(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//        } finally {
//            TenantInfoHolder.clear();
//        }
//    }
//
//    @Override
//    public ApiResult<ImMsgSendResultDTO> sendAutomationNotification(WorkWechatNotificationDTO dto) {
//
//        log.debug("sendAutomationNotification, dto = {}", dto);
//        try {
//            if (CollectionUtils.isEmpty(dto.getUserIds())){
//                return ApiResult.success(new ImMsgSendResultDTO(Boolean.FALSE, CurrentV2.Error.NOTIFY_NOBODY_OR_EMAIL_IM_NOT_BIND));
//            }
//            ImOrgAppVO imOrgAppVO = notificationSender.getImOrgApp(dto.getTenantId());
//            if (imOrgAppVO == null || imOrgAppVO.getImOrg() == null || imOrgAppVO.getImApp() == null){
//                log.warn("sendAutomationNotification failed, im config not found! dto = {}", dto);
//                return ApiResult.error(new ImMsgSendResultDTO(Boolean.FALSE, CurrentV2.Error.NOTIFY_NOBODY_OR_EMAIL_IM_NOT_BIND), BizRespStatusEnum.IM_ORG_NOT_EXISTS);
//            }
//
//            //创建消息记录对象
//            IntegrationExternalMsg externalMsg = createExternalMsg(dto, imOrgAppVO.getImApp());
//            Map<String, Long> receiverMap = notificationSender.getExternalUserIds(dto, imOrgAppVO);
//            if (MapUtils.isEmpty(receiverMap)){
//                return ApiResult.success(new ImMsgSendResultDTO(Boolean.FALSE, CurrentV2.Error.NOTIFY_NOBODY_OR_EMAIL_IM_NOT_BIND));
//            }
//            List<String> receivers = receiverMap.keySet().stream().collect(Collectors.toList());
//            List<IntegrationExternalMsgReceiver> externalMsgReceivers = createMsgReceiver(externalMsg, receivers);
//            //存储消息记录
//            insertRecordUnderTransactional(dto.getTenantId(), externalMsg, externalMsgReceivers);
//            //发送消息
//            notificationSender.sendTextCardMsg(dto, imOrgAppVO, receiverMap, x -> {
//                // 填充更新信息
//                IntegrationExternalMsg toUpdateExternalMsg = new IntegrationExternalMsg();
//                toUpdateExternalMsg.setExternalOpenMsgId(x.getMsgid());
//                toUpdateExternalMsg.setExternalCreateDt(new Date());
//                toUpdateExternalMsg.setUpdateBy(dto.getTriggerUserId());
//                toUpdateExternalMsg.setUpdateTime(toUpdateExternalMsg.getExternalCreateDt());
//                // 执行更新
//                Example example = new Example.Builder(IntegrationExternalMsg.class).andWhere(
//                        WeekendSqls.<IntegrationExternalMsg>custom()
//                                .andEqualTo(IntegrationExternalMsg::getId, externalMsg.getId())
//                                .andEqualTo(IntegrationExternalMsg::getDeleteFlag, DeleteFlagEnum.NO.getCode())
//                ).build();
//                MultiTenantUtil.execUnderTenantEnv(
//                        dto.getTenantId(),
//                        () -> integrationExternalMsgManager.updateByExampleSelective(toUpdateExternalMsg, example)
//                );
//            });
//        } catch (Exception e){
//            log.error("sendAutomationNotification failed， dto = " + dto, e);
//            return ApiResult.error(new ImMsgSendResultDTO(Boolean.FALSE, CurrentV2.Error.SYS_ERROR), BizRespStatusEnum.IM_EXCEPTION);
//        }
//        return ApiResult.success(new ImMsgSendResultDTO(Boolean.TRUE));
//    }
//
//    @Override
//    public ApiResult<ImMsgSendResultDTO> sendAutomationNotificationBlank(ImNotificationBlankDTO imDTO) {
//        log.debug("sendAutomationNotificationBlank, imDTO = {}", imDTO);
//        try {
//            if (CollectionUtils.isEmpty(imDTO.getUserIds())) {
//                return ApiResult.success(null);
//            }
//            ImOrgAppVO imOrgAppVO = notificationSender.getImOrgApp(imDTO.getTenantId());
//            if (imOrgAppVO == null || imOrgAppVO.getImOrg() == null || imOrgAppVO.getImApp() == null) {
//                log.warn("sendAutomationNotificationBlank failed, im config not found! imDTO = {}", imDTO);
//                return ApiResult.error(new ImMsgSendResultDTO(Boolean.FALSE, CurrentV2.Error.NOTIFY_NOBODY_OR_EMAIL_IM_NOT_BIND),
//                        BizRespStatusEnum.IM_ORG_NOT_EXISTS);
//            }
//
//            // 创建消息记录对象
//            IntegrationExternalMsg externalBlankMsg = createExternalBlankMsg(imDTO, imOrgAppVO.getImApp());
//            // 获取外部用户
//            WorkWechatNotificationDTO workWechatNotificationDTO = WorkWechatNotificationDTO.builder()
//                    .userIds(imDTO.getUserIds())
//                    .type(NotificationType.AUTOMATION)
//                    .tenantId(imDTO.getTenantId())
//                    .build();
//            Map<String, Long> receiverMap = notificationSender.getExternalUserIds(workWechatNotificationDTO, imOrgAppVO);
//            if (MapUtils.isEmpty(receiverMap)) {
//                return ApiResult.success(new ImMsgSendResultDTO(Boolean.FALSE, CurrentV2.Error.NOTIFY_NOBODY_OR_EMAIL_IM_NOT_BIND));
//            }
//            List<String> receivers = receiverMap.keySet().stream().collect(Collectors.toList());
//            List<IntegrationExternalMsgReceiver> externalMsgReceivers = createMsgReceiver(externalBlankMsg, receivers);
//            // 存储消息记录
//            insertRecordUnderTransactional(imDTO.getTenantId(), externalBlankMsg, externalMsgReceivers);
//            // 发送消息
//            notificationSender.sendMarkdownMsg(imDTO, imOrgAppVO, receiverMap, x -> {
//                // 填充更新信息
//                IntegrationExternalMsg toUpdateExternalMsg = new IntegrationExternalMsg();
//                toUpdateExternalMsg.setExternalOpenMsgId(x.getMsgid());
//                toUpdateExternalMsg.setExternalCreateDt(new Date());
//                toUpdateExternalMsg.setUpdateBy(imDTO.getUpdateBy());
//                toUpdateExternalMsg.setUpdateTime(toUpdateExternalMsg.getExternalCreateDt());
//                // 执行更新
//                Example example = new Example.Builder(IntegrationExternalMsg.class).andWhere(
//                        WeekendSqls.<IntegrationExternalMsg>custom()
//                                .andEqualTo(IntegrationExternalMsg::getId, externalBlankMsg.getId())
//                                .andEqualTo(IntegrationExternalMsg::getDeleteFlag, DeleteFlagEnum.NO.getCode())
//                ).build();
//                MultiTenantUtil.execUnderTenantEnv(
//                        imDTO.getTenantId(),
//                        () -> integrationExternalMsgManager.updateByExampleSelective(toUpdateExternalMsg, example)
//                );
//            });
//        } catch (Exception e){
//            log.error("sendAutomationNotificationBlank failed， imDTO = " + imDTO, e);
//            return ApiResult.error(new ImMsgSendResultDTO(Boolean.FALSE, CurrentV2.Error.SYS_ERROR), BizRespStatusEnum.IM_EXCEPTION);
//        }
//        return ApiResult.success(new ImMsgSendResultDTO(Boolean.TRUE));
//    }
//
//    @Override
//    public ApiResult<Void> asyncExportUser(CallbackConfigBo callbackConfigBo) {
//        ExportTaskManager.asyncExportUser(callbackConfigBo);
//        return ApiResult.success(null);
//    }
//
//    @Override
//    public ApiResult<Void> asyncStoreExportUser(CallbackConfigBo callbackConfigBo) {
//        String jobId = ExportTaskStoreManager.asyncExportUser(callbackConfigBo);
//        // 由于导出任务没有通讯录回调通知-异步任务完成通知的回调，所以需要在这里手动触发一次
//        executorService.execute(() -> {
//            try {
//                Thread.sleep(5000);
//                BatchJobResultEvent event = new BatchJobResultEvent();
//                event.setToUserName(callbackConfigBo.getCorpId());
//                event.setEvent(EventTypeEnum.BATCH_JOB_RESULT.getType());
//                event.setBatchJob(new BatchJobResultEvent.BatchJob(jobId, BatchJobResultEvent.JobType.export_user));
//                batchJobResultStoreEventHandler.process(event);
//            } catch (InterruptedException e) {
//                log.error("asyncStoreExportUser sleep error", e);
//            }
//        });
//        return ApiResult.success(null);
//    }
//
//    @Override
//    public ApiResult<SimpleExpireKey> getAccessToken(ImOrgAppAddDTO dto) {
//        Optional<AccessTokenVO> accessTokenOp = Optional.empty();
//        if (AppType.STORE.getCode().equals(dto.getAppType())) {
//            ApiResult<String> suiteAccessTokenRes = SuiteAccessTokenManager.getToken(AppType.getTypeByCode(dto.getAppType()));
//            if (suiteAccessTokenRes.isSuccess()) {
//                accessTokenOp = WorkWechatSAO.getCorpToken(suiteAccessTokenRes.getResult(),
//                        AuthInfoDTO.builder().auth_corpid(dto.getExternalOrgId()).permanent_code(dto.getAppSecret()).build());
//            }
//        } else {
//            accessTokenOp = WorkWechatSAO.getToken(new AccessTokenDTO(dto.getExternalOrgId(), dto.getAppSecret()));
//        }
//        if (accessTokenOp.isPresent()){
//            SimpleExpireKey simpleExpireKey = new SimpleExpireKey(accessTokenOp.get().getAccess_token(),
//                    Long.valueOf(accessTokenOp.get().getExpires_in()) - 300);
//            return ApiResult.success(simpleExpireKey);
//        }
//        throw new BizException(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//    }
//
//    @Override
//    public ApiResult<SimpleExpireKey> getJsApiTicket(String accessToken, String ticketType) {
//        Optional<JsTicketVO> jsTicketVO = WorkWechatSAO.getJsTicket(accessToken, ticketType);
//        if (jsTicketVO.isPresent()){
//            SimpleExpireKey simpleExpireKey = new SimpleExpireKey(jsTicketVO.get().getTicket(),
//                    Long.valueOf(jsTicketVO.get().getExpires_in()) - 300);
//            return ApiResult.success(simpleExpireKey);
//        }
//        throw new BizException(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//    }
//
//    @Override
//    public ApiResult<ImAppConfigVO> getWorkWechatAppConfig(ImAppConfigGetDTO dto) {
//        try {
//            Long tenantId = Long.valueOf(TenantInfoHolder.getTenantInfo().getTenantId());
//            Organization organization = organizationManager.getOrganization(tenantId);
//            if (organization == null || StringUtil.isEmpty(organization.getUuid())){
//                return ApiResult.error(BizRespStatusEnum.USER_ORG_NOT_EXISTS);
//            }
//            String h5PcHomeUrl = workWechatAppConfig.getH5PcHomeUrl();
//            String h5CredibleDomainUrl = workWechatAppConfig.getH5CredibleDomainUrl();
//            String eventRequestUrl = workWechatAppConfig.getEventRequestUrl();
//            List<String> credibleIpGroup = workWechatAppConfig.getCredibleIpGroup();
//            String authQrCodeImgUrl = workWechatAppConfig.getAuthQrCodeImgUrl();
//
//            ImOrgAppVO imOrgApp = integrationImOrgManager.selectImOrgApp(new ImOrgAppSelectDTO(ImType.WORK_WECHAT.getCode()));
//            // 返回代开发应用配置（代开发应用集成页获取配置时应用可能还未创建）
//            ImAppConfigVO.Builder builder = new ImAppConfigVO.Builder().h5PcHomeUrl(h5PcHomeUrl).h5CredibleDomainUrl(h5CredibleDomainUrl)
//                    .eventRequestUrl(eventRequestUrl.replace("${orgId}", organization.getUuid()))
//                    .appStoreJumpUrl(workWechatStoreAppConfig.getAppStoreJumpUrl());
//            // TODO AB开关临时处理
//            if (imOrgApp == null) {
//                ApiResult<JSONObject> featuresForUser = flagsmithService.getFeaturesForUser();
//                if (featuresForUser.isSuccess() && ObjectUtils.isNotEmpty(featuresForUser.getResult()) && Boolean.TRUE.equals(
//                        featuresForUser.getResult().getBoolean(FeatureEnum.WORK_WECHAT_STORE_SWITCH.getKey()))) {
//                    builder.quickOperationPcUrl(workWechatStoreAppConfig.getQuickOperationPcUrl());
//                } else {
//                    builder.authQrCodeImgUrl(authQrCodeImgUrl);
//                }
//                return ApiResult.success(builder.build());
//            }
//            if (imOrgApp.isStoreApp()) {
//                builder.quickOperationPcUrl(workWechatStoreAppConfig.getQuickOperationPcUrl());
//            } else if (imOrgApp.isWeAgentApp()) {
//                builder.authQrCodeImgUrl(authQrCodeImgUrl)
//                        .quickOperationPcUrl(workWechatAppConfig.getQuickOperationPcUrl().replace("${orgId}", organization.getUuid()));
//            } else {
//                builder.token(imOrgApp.getImApp().getCallbackToken())
//                        .encodingAesKey(imOrgApp.getImApp().getCallbackKey())
//                        .credibleIpGroup(credibleIpGroup);
//            }
//            return ApiResult.success(builder.build());
//        } catch (Exception e) {
//            log.error("IntegrationImServiceImpl.getConfig(ImAppConfigDTO) exception: ", e);
//            return ApiResult.error(BizRespStatusEnum.IM_EXCEPTION);
//        }
//    }
//
//    @Override
//    public ApiResult<Boolean> updateAppState(ImAppStateDTO dto) {
//        // 查询组织企微应用
//        ImOrgAppVO imOrgAppVO = ApiResultUtil.requiredSuccessResult(
//                integrationImOrgService.get(new ImOrgAppSelectDTO(ImType.WORK_WECHAT.getCode()))
//        );
//        // 非代开发应用，不允许修改
//        ImAppVO imAppVO = imOrgAppVO.getImApp();
//        if (!AppType.AGENT.getCode().equals(imAppVO.getAppType())) {
//            return ApiResult.error(BizRespStatusEnum.COM_ILLEGAL_OPERATION);
//        }
//        Byte state = imAppVO.getState();
//        Long userId = UserContextUtil.get().getUserId();
//        // 状态流转
//        IntegrationImApp imApp = new IntegrationImApp();
//        imApp.setId(imAppVO.getImAppId());
//        int updateCnt = 0;
//        if (WorkWechatImState.UNAUTHORIZED.getState().equals(dto.getAppState())) {
//            // 未授权，验证是否已授权
//            if (!WorkWechatImState.AUTHORIZED.getState().equals(state)) {
//                return ApiResult.success(false);
//            }
//        } else if (WorkWechatImState.ONLINE.getState().equals(dto.getAppState())) {
//            // 已上线 -> 配置应用可见范围
//            if (!WorkWechatImState.ONLINE.getState().equals(state)) {
//                return ApiResult.success(false);
//            }
//            imApp.setState(WorkWechatImState.CONFIGURING.getState());
//            imApp.setUpdateBy(userId);
//            imApp.setUpdateTime(new Date());
//            updateCnt += integrationImAppManager.updateByPrimaryKeySelective(imApp);
//        } else if (WorkWechatImState.CONFIGURING.getState().equals(dto.getAppState())) {
//            // 配置应用可见范围 -> 配置快速创建页面（验证可见范围是否配置完成）
//            if (!WorkWechatImState.CONFIGURED.getState().equals(state)) {
//                String authInfoJsonTxt = imAppVO.getAuthInfoJsonTxt();
//                AuthInfoVO authInfoVO = JSON.parseObject(authInfoJsonTxt, AuthInfoVO.class);
//                Optional<AuthInfoVO.Privilege> privilegeOptional =
//                        Optional.ofNullable(authInfoVO).map(AuthInfoVO::getAuthInfo).map(AuthInfoVO.AuthInfo::getAgent).map(map -> map.get(0))
//                                .map(AuthInfoVO.Agent::getPrivilege);
//                if (privilegeOptional.isEmpty() || !privilegeOptional.get().hasPrivilegeRange()) {
//                    return ApiResult.success(false);
//                }
//            }
//            if (WorkWechatImState.QUICK_OPERATION.getState().equals(state)) {
//                return ApiResult.success(true);
//            }
//            imApp.setState(WorkWechatImState.QUICK_OPERATION.getState());
//            imApp.setUpdateBy(userId);
//            imApp.setUpdateTime(new Date());
//            updateCnt += integrationImAppManager.updateByPrimaryKeySelective(imApp);
//        } else if (WorkWechatImState.QUICK_OPERATION.getState().equals(dto.getAppState())) {
//            if (!WorkWechatImState.QUICK_OPERATION.getState().equals(state)) {
//                return ApiResult.success(false);
//            }
//            // 发送欢迎消息
//            OpenImFactory.Type.getTypeByCode(ImType.WORK_WECHAT.getCode()).getOpenImFactory().sendWelcomeMsg();
//            // 配置快速创建页面 -> 完成配置
//            imApp.setState(WorkWechatImState.CONFIGURED.getState());
//            imApp.setUpdateBy(userId);
//            imApp.setUpdateTime(new Date());
//            updateCnt += integrationImAppManager.updateByPrimaryKeySelective(imApp);
//        } else if (WorkWechatImState.CONFIGURED.getState().equals(dto.getAppState())) {
//            // 完成配置 -> 已完成
//            if (!WorkWechatImState.CONFIGURED.getState().equals(state)) {
//                return ApiResult.success(false);
//            }
//            imApp.setState(WorkWechatImState.FINISHED.getState());
//            imApp.setUpdateBy(userId);
//            imApp.setUpdateTime(new Date());
//            updateCnt += integrationImAppManager.updateByPrimaryKeySelective(imApp);
//            // 发送集成通知
//            integrationStationMessageService.sendEnableNotice(
//                    StationNotifyDTO.builder()
//                            .type(IntegrationTypeEnum.WORK_WECHAT)
//                            .objectId(imAppVO.getImOrgId())
//                            .notifyAccountIdSet(new HashSet<>())
//                            .build());
//        }
//        // 更新成功，删除缓存
//        if (updateCnt > 0) {
//            integrationImOrgManager.removeImOrgCache(imOrgAppVO.getTenantId(), imOrgAppVO.getImOrg().getExternalOrgId(),
//                    ImType.WORK_WECHAT.getCode(), imOrgAppVO.getImApp().getAppType());
//            integrationImAppManager.removeImAppCache(imOrgAppVO.getTenantId(), ImType.WORK_WECHAT.getCode(), imAppVO.getAppId());
//        }
//        return ApiResult.success(true);
//    }
//
//    @Override
//    public ApiResult<AuthInfoVO> getAuthInfo(String appType, String openCorpId) {
//        try {
//            // 查询授权信息记录
//            ApiResult<IntegrationWxAuthRecord> apiResult =
//                    integrationWxAuthRecordService.selectOneByOpenCorpId(appType, openCorpId);
//            IntegrationWxAuthRecord integrationWxAuthRecord = apiResult.getResult();
//            if (integrationWxAuthRecord == null) {
//                log.error("IntegrationImServiceImpl.getAuthInfo() integrationWxAuthRecord is null, openCorpId : {}", openCorpId);
//                return ApiResult.error(BizCodeIntegrationEnum.IM_APP_UNAUTHORIZED);
//            }
//            String permanentCode = integrationWxAuthRecord.getPermanentCode();
//            // 获取suiteAccessToken
//            ApiResult<String> tokenRes = SuiteAccessTokenManager.getToken(AppType.getTypeByCode(appType));
//            if (!tokenRes.isSuccess()) {
//                log.warn("getUserInfoThird get suiteAccessToken fail");
//                return ApiResult.error(tokenRes);
//            }
//            // 获取企业授权信息
//            Optional<com.zhikan.agile.pms.workwechat.core.vo.AuthInfoVO> authInfoOptional = WorkWechatSAO.getAuthInfo(tokenRes.getResult(),
//                    AuthInfoDTO.builder().auth_corpid(openCorpId).permanent_code(permanentCode).build());
//            if (authInfoOptional.isPresent()) {
//                AuthInfoVO authInfoVO = JSON.parseObject(JSON.toJSONString(authInfoOptional.get()), AuthInfoVO.class);
//                authInfoVO.setPermanentCode(permanentCode);
//                return ApiResult.success(authInfoVO);
//            }
//        } catch (Exception e) {
//            log.error("IntegrationImServiceImpl.getAuthInfo() exception: ", e);
//            return ApiResult.error(BizRespStatusEnum.IM_EXCEPTION);
//        }
//        return ApiResult.error(BizRespStatusEnum.IM_EXCEPTION);
//    }
//
//    @Override
//    @CheckParam
//    public ApiResult<Void> sendTextMsg(
//            @NonNull Collection<WorkWechatEndpoint> endpoints, @NonNull Collection<WorkWechatSendDTO> messages) {
//        ApiResult<ImOrgAppVO> imOrgAppVoApiResult = integrationImOrgService.get(new ImOrgAppSelectDTO(ImType.WORK_WECHAT.getCode()));
//        if (!imOrgAppVoApiResult.isSuccess() || imOrgAppVoApiResult.getResult() == null || imOrgAppVoApiResult.getResult().getImApp() == null){
//            throw new BizException(BizRespStatusEnum.OPEN_IM_NOT_EXISTS);
//        }
//        String appType = imOrgAppVoApiResult.getResult().getImApp().getAppType();
//        endpoints.forEach(endpoint -> {
//            WorkWechatEndpoint.App app = endpoint.getApp();
//            String toUser = endpoint.getUserIds().stream().distinct().collect(Collectors.joining("|"));
//            messages.forEach(message -> {
//                TextCardMsgDTO msg = CglibBeanCopyX.copyProperties(message, TextCardMsgDTO::new);
//                String msgType = msg.getMsgType();
//                MsgDTO messageBody = MsgDTO.builder()
//                        .agentid(app.getAppId())
//                        .msgtype(msgType)
//                        .touser(toUser)
//                        .build();
//                JSONObject jsonObject = (JSONObject) JSON.toJSON(messageBody);
//                jsonObject.put(msgType, JSON.toJSON(msg));
//                com.zhikan.agile.pms.common.utils.imports.v2.Requires.require(
//                        WorkWechatSAO.sendMsg(
//                                AccessTokenManager.getTokenByType(appType, app.getOrgId(), app.getAppId(), app.getAppSecret()),
//                                jsonObject),
//                        Optional::isPresent
//                );
//            });
//        });
//        return ApiResult.success(null);
//    }
//
//    @Override
//    public ApiResult<Void> sendMarkdownMsg(
//            @NonNull Collection<WorkWechatEndpoint> endpoints, @NonNull Collection<WorkWechatSendMarkdownDTO> messages) {
//        ApiResult<ImOrgAppVO> imOrgAppVoApiResult = integrationImOrgService.get(new ImOrgAppSelectDTO(ImType.WORK_WECHAT.getCode()));
//        if (!imOrgAppVoApiResult.isSuccess() || imOrgAppVoApiResult.getResult() == null || imOrgAppVoApiResult.getResult().getImApp() == null){
//            throw new BizException(BizRespStatusEnum.OPEN_IM_NOT_EXISTS);
//        }
//        String appType = imOrgAppVoApiResult.getResult().getImApp().getAppType();
//        endpoints.forEach(endpoint -> {
//            WorkWechatEndpoint.App app = endpoint.getApp();
//            String toUser = endpoint.getUserIds().stream().distinct().collect(Collectors.joining("|"));
//            messages.forEach(message -> {
//                MarkdownMsgDTO msg = CglibBeanCopyX.copyProperties(message, MarkdownMsgDTO::new);
//                String msgType = msg.getMsgType();
//                MsgDTO messageBody = MsgDTO.builder()
//                        .agentid(app.getAppId())
//                        .msgtype(msgType)
//                        .touser(toUser)
//                        .build();
//                JSONObject jsonObject = (JSONObject) JSON.toJSON(messageBody);
//                jsonObject.put(msgType, JSON.toJSON(msg));
//                com.zhikan.agile.pms.common.utils.imports.v2.Requires.require(
//                        WorkWechatSAO.sendMsg(
//                                AccessTokenManager.getTokenByType(appType, app.getOrgId(), app.getAppId(), app.getAppSecret()),
//                                jsonObject),
//                        Optional::isPresent
//                );
//            });
//        });
//        return ApiResult.success(null);
//    }
//
//    @Override
//    public ApiResult<Void> sendFeatureGuideMsg(ImOrgAppVO imOrgAppVO, String receiverUser) {
//        String corpId = imOrgAppVO.getImOrg().getExternalOrgId();
//        String agentId = imOrgAppVO.getImApp().getAppId();
//        // 获取企业凭证
//        ApiResult<String> corpAccessTokenRes = CorpAccessTokenManager.getToken(corpId,
//                imOrgAppVO.getImApp().getAppId(), imOrgAppVO.getImApp().getAppSecret());
//        if (!corpAccessTokenRes.isSuccess()) {
//            log.error("WorkWechatServiceImpl.sendStoreMsg() get corp access token fail, corpId : {}", corpId);
//            return ApiResult.error(corpAccessTokenRes);
//        }
//        JSONObject jsonObject = specialMsgProducer.makeFeatureGuideMsg(agentId, receiverUser);
//        // 发送消息
//        Optional<MsgVO> msgVO = WorkWechatSAO.sendMsg(corpAccessTokenRes.getResult(), jsonObject);
//        if (msgVO.isEmpty()) {
//            log.error("WorkWechatServiceImpl.sendFeatureGuideMsg() send msg fail, corpId : {}", corpId);
//            return ApiResult.error(BizRespStatusEnum.OPEN_IM_EXCEPTION);
//        }
//        externalMsgHelper.insertOpGuideMsg(imOrgAppVO, receiverUser, msgVO.get());
//        integrationSensorHelperV2.trackImOpGuideMsgWithReason(ImType.WORK_WECHAT, 1, OpReasonEnum.LOGIN_SUCCESS);
//        return ApiResult.success(null);
//    }
//
//    @Override
//    public ApiResult<Void> sendCancelBindTenantMsg(String corpId, ImOrgAppVO imOrgAppVO, List<ImUserVO> imUserVOList, Boolean sendExternalMsg) {
//        log.info("WorkWechatServiceImpl.sendCancelBindTenantMsg() corpId : {}, sendExternalMsg : {}", corpId, sendExternalMsg);
//        try {
//            // 查询有企微集成权限的角色及全部用户
//            List<Long> roleIds = permissionMgrService.queryHasPermissionRoleIds(ResourcePermissionEnum.IntegrationWorkWechat);
//            List<String> roleCodeSortList = Arrays.asList(SystemRoleCodeEnum.ORG_OWNER.getCode(), SystemRoleCodeEnum.ORG_ADMIN.getCode(), SystemRoleCodeEnum.MEMBER.getCode());
//            List<AccountSimpleVO> accountSimpleVoList = accountService.queryByRoleCodes(roleCodeSortList).getResult();
//            // 筛选有权限的用户并排序（顺序是组织拥有者>组织管理员>其他具有集成权限的用户，按名称排序）
//            List<String> adminNameList = StreamX.safe(accountSimpleVoList)
//                    .filter(vo -> roleIds.contains(Long.valueOf(vo.getRoleId())))
//                    .sorted(Comparator.comparing(AccountSimpleVO::getRoleCode, Comparator.comparing(roleCodeSortList::indexOf)).thenComparing(AccountSimpleVO::getName))
//                    .map(AccountSimpleVO::getName).distinct().toList();
//
//            Map<String, Long> authedUserMap = StreamX.safe(imUserVOList)
//                    .collect(Collectors.toMap(ImUserVO::getExternalUserId, ImUserVO::getInternalUserId));
//            // 查询im用户价值信息
//            ApiResult<Map<Long, ImUserWorthVO>> imUserWorthVoRes = imWorthService.getImUserWorthVO(imOrgAppVO.getImType(), authedUserMap.values(), imOrgAppVO.getCreateTime());
//            if (!imUserWorthVoRes.isSuccess()) {
//                log.error("WorkWechatServiceImpl.sendCancelBindTenantMsg() getImUserWorthVO fail");
//                return ApiResult.error(imUserWorthVoRes);
//            }
//
//            if (sendExternalMsg) {
//                // 获取企业凭证
//                ApiResult<String> corpAccessTokenRes = CorpAccessTokenManager.getToken(imOrgAppVO);
//                if (!corpAccessTokenRes.isSuccess()) {
//                    log.warn("WorkWechatServiceImpl.sendCancelBindTenantMsg() get corp access token fail, corpId : {}", corpId);
//                    return ApiResult.error(corpAccessTokenRes);
//                }
//                // 操作人信息
//                ApiResult<AccountSimpleVO> accountSimpleVO = accountService.getAccountSimpleVO(UserContextUtil.getUserId());
//                // 拼接管理员
//                String adminUsers = StreamX.safe(adminNameList).limit(5).collect(Collectors.joining("、"));
//                if (adminNameList.size() > 5) {
//                    adminUsers += "...";
//                }
//                final String finalAdminUsers = adminUsers;
//                authedUserMap.forEach((externalUserId, internalUserId) -> {
//                    // 构建消息
//                    ImUserWorthVO imUserWorthVO = imUserWorthVoRes.getResult().get(internalUserId);
//                    JSONObject jsonObject = specialMsgProducer.makeCancelBindTenantMsg(accountSimpleVO.getResult().getName(),
//                            imOrgAppVO.getImApp().getAppId(), externalUserId, finalAdminUsers, imUserWorthVO != null ? imUserWorthVO : new ImUserWorthVO());
//                    // 发送消息
//                    Optional<MsgVO> msgVO = WorkWechatSAO.sendMsg(corpAccessTokenRes.getResult(), jsonObject);
//                    log.info("WorkWechatServiceImpl.sendCancelBindTenantMsg(), send msg, corpId : {}, result : {}", corpId, msgVO);
//                });
//                // 埋点处理
//                integrationSensorHelperV2.trackImDisabledMsg(ImType.WORK_WECHAT, authedUserMap.size(), ReceivePlatformEnum.WORK_WECHAT);
//            }
//            // 发送站内消息
//            integrationStationMessageService.sendDisableNotice(
//                    StationNotifyDTO.builder()
//                            .type(IntegrationTypeEnum.WORK_WECHAT)
//                            .objectId(imOrgAppVO.getImOrgId())
//                            .notifyAccountIdSet(new HashSet<>(authedUserMap.values()))
//                            .startTime(imOrgAppVO.getCreateTime())
//                            .build());
//            // 埋点处理
//            integrationSensorHelperV2.trackImDisabledMsg(ImType.WORK_WECHAT, authedUserMap.size(), ReceivePlatformEnum.STATION);
//            log.info("WorkWechatServiceImpl.sendCancelBindTenantMsg(), send station unbind im notice, corpId: {}", corpId);
//            return ApiResult.success(null);
//        } catch (Exception e) {
//            log.error("WorkWechatServiceImpl.sendCancelBindTenantMsg() error, corpId : {}", corpId, e);
//            return ApiResult.error(BizRespStatusEnum.IM_EXCEPTION);
//        }
//    }
//
//    /**
//     * 生成新的外部消息
//     * @param dto
//     * @param imAppVO
//     * @return
//     */
//    private IntegrationExternalMsg createExternalMsg( WorkWechatNotificationDTO dto, ImAppVO imAppVO) {
//        IntegrationExternalMsg newExternalMsg = new IntegrationExternalMsg();
//        newExternalMsg.setTransactionId(idManager.getSegmentID());
//        newExternalMsg.setId(idManager.getSegmentID());
//        newExternalMsg.setTypeCd(IntegrationExternalMsgHelper.Type.WORK_WECHAT.getCode());
//        newExternalMsg.setSenderTypeCd(IntegrationExternalMsgHelper.SenderType.APP.getCode());
//        newExternalMsg.setSenderIdTypeCd(IntegrationExternalMsgHelper.SenderIdType.APP_ID.getCode());
//        newExternalMsg.setSenderId(imAppVO.getAppId());
//        newExternalMsg.setInitDataJsonTxt(JSON.toJSONString(dto));
//        newExternalMsg.setCreateBy(dto.getTriggerUserId());
//        newExternalMsg.setImAppId(imAppVO.getImAppId());
//        newExternalMsg.setCreateTime(new Date());
//        newExternalMsg.setUpdateBy(newExternalMsg.getCreateBy());
//        newExternalMsg.setUpdateTime(newExternalMsg.getCreateTime());
//        newExternalMsg.setDeleteFlag(DeleteFlagEnum.NO.getCode());
//        newExternalMsg.setTenantId(dto.getTenantId());
//        return newExternalMsg;
//    }
//
//    /**
//     * 生成新的外部消息-Blank
//     * @param dto
//     * @param imAppVO
//     * @return
//     */
//    private IntegrationExternalMsg createExternalBlankMsg(ImNotificationBlankDTO dto, ImAppVO imAppVO) {
//        IntegrationExternalMsg newExternalMsg = new IntegrationExternalMsg();
//        newExternalMsg.setTransactionId(idManager.getSegmentID());
//        newExternalMsg.setId(idManager.getSegmentID());
//        newExternalMsg.setTypeCd(IntegrationExternalMsgHelper.Type.WORK_WECHAT.getCode());
//        newExternalMsg.setSenderTypeCd(IntegrationExternalMsgHelper.SenderType.APP.getCode());
//        newExternalMsg.setSenderIdTypeCd(IntegrationExternalMsgHelper.SenderIdType.APP_ID.getCode());
//        newExternalMsg.setSenderId(imAppVO.getAppId());
//        newExternalMsg.setInitDataJsonTxt(JSON.toJSONString(dto));
//        newExternalMsg.setImAppId(imAppVO.getImAppId());
//        newExternalMsg.setCreateBy(dto.getCreateBy());
//        newExternalMsg.setCreateTime(new Date());
//        newExternalMsg.setUpdateBy(newExternalMsg.getCreateBy());
//        newExternalMsg.setUpdateTime(newExternalMsg.getCreateTime());
//        newExternalMsg.setDeleteFlag(DeleteFlagEnum.NO.getCode());
//        newExternalMsg.setTenantId(dto.getTenantId());
//        return newExternalMsg;
//    }
//
//    /**
//     * 生产新的外部信息接受者
//     * @param externalMsg
//     * @param receiverUserIdSet
//     * @return
//     */
//    private List<IntegrationExternalMsgReceiver> createMsgReceiver(IntegrationExternalMsg externalMsg, List<String> receiverUserIdSet) {
//        return  receiverUserIdSet.stream().map(x -> {
//            IntegrationExternalMsgReceiver newMsgReceiver = CglibBeanCopyUtil.copyProperties(
//                    externalMsg, IntegrationExternalMsgReceiver.class
//            );
//            newMsgReceiver.setId(idManager.getSegmentID());
//            newMsgReceiver.setExternalMsgId(externalMsg.getId());
//            newMsgReceiver.setExternalReceiverIdTypeCd(ReceiveIdType.USER_ID.getCode());
//            newMsgReceiver.setExternalReceiverId(x);
//            newMsgReceiver.setReceived(IntegrationExternalMsgReceiverHelper.Received.YES.getCode());
//            return newMsgReceiver;
//        }).collect(Collectors.toList());
//    }
//
//    /**
//     * 事务下新增消息记录
//     *
//     * @param tenantId 租户ID
//     * @param toInsertExternalMsg 待新增外部消息
//     * @param toInsertMsgReceivers 待新增消息接收者
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public void insertRecordUnderTransactional(Long tenantId, IntegrationExternalMsg toInsertExternalMsg,
//                                               List<IntegrationExternalMsgReceiver> toInsertMsgReceivers) {
//        // 新增消息记录
//        MultiTenantUtil.execUnderTenantEnv(
//                tenantId,
//                () -> {
//                    Requires.require(integrationExternalMsgManager.insertSelective(toInsertExternalMsg), r -> r > 0);
//                    Requires.require(integrationExternalMsgReceiverManager.insertList(toInsertMsgReceivers), r -> r > 0);
//                }
//        );
//    }
//
//    /**
//     * 获取auth2.0重定向地址（ligaai）
//     * @param workWechatAuthURLDTO
//     * @return
//     */
//    private String getRedirectUrl(WorkWechatAuthUrlDTO workWechatAuthURLDTO){
//        return UrlUtil.getUrlWithParams(workWechatAppConfig.getAuthRedirectUrl(), Map.of("orgId", workWechatAuthURLDTO.getOrgId(),
//                "originalPath", workWechatAuthURLDTO.getOriginalPath()));
//    }
//
//    private void cacheState(String uuidState){
//        stringRedisService.set(AUTH_STATE_KEY_PREF + uuidState, uuidState, EXPIRE);
//    }
//
//    private void removeState(String uuidState){
//        stringRedisService.delete(AUTH_STATE_KEY_PREF + uuidState);
//    }
//
//    private boolean checkState(String uuidState){
//        if (StringUtils.isBlank(uuidState)){
//            return Boolean.FALSE;
//        }
//        String cachedState = stringRedisService.get(AUTH_STATE_KEY_PREF +uuidState);
//        return uuidState.equals(cachedState);
//    }
//
//    /**
//     * 获取组织
//     * @param orgId
//     * @return
//     */
//    private Organization getOrganization(String orgId) {
//        return organizationManager.selectByUuid(orgId);
//    }
//
//    private void setLocalParam(Long tenantId) {
//        DefaultTenantInfo defaultTenantInfo = new DefaultTenantInfo();
//        defaultTenantInfo.setTenantId(String.valueOf(tenantId));
//        defaultTenantInfo.setTenantIdColumn("tenant_id");
//        TenantInfoHolder.setTenantInfo(defaultTenantInfo);
//    }
//
//    private ImOrgAppVO getImOrgApp(String orgUuid){
//        Organization organization = getOrganization(orgUuid);
//        if (organization == null){
//            throw new BizException(BizRespStatusEnum.OPEN_IM_NOT_EXISTS);
//        }
//        this.setLocalParam(organization.getTenantId());
//        ApiResult<ImOrgAppVO> imOrgAppVoApiResult = integrationImOrgService.get(new ImOrgAppSelectDTO(ImType.WORK_WECHAT.getCode()));
//        if (!imOrgAppVoApiResult.isSuccess() || imOrgAppVoApiResult.getResult() == null || imOrgAppVoApiResult.getResult().getImOrg() == null){
//            throw new BizException(BizRespStatusEnum.OPEN_IM_NOT_EXISTS);
//        }
//        imOrgAppVoApiResult.getResult().setTenantId(organization.getTenantId());
//        return imOrgAppVoApiResult.getResult();
//    }
//
//    @Override
//    public ApiResult<Boolean> checkProductRight(Boolean need, Long tenantId) {
//        if (need == null || !need){
//            return ApiResult.success(Boolean.TRUE);
//        }
//        OrgProductRight orgProductRight = orgProductRightManager.queryOrgProductRight(ProductLineEnum.PMS.getTypeCode());
//        Map<Long, Map<String, ProductRuleValueVO>> productSchemeRuleMap =
//                productSchemeRuleManager.productSchemeRuleMap(new ProductLineIdDTO(ProductLineEnum.PMS.getTypeCode()));
//        if (productSchemeRuleMap != null) {
//            Map<String, ProductRuleValueVO> schemeIdProductRuleValueMap = productSchemeRuleMap.get(orgProductRight.getProductSchemeId());
//            if (schemeIdProductRuleValueMap != null) {
//                ProductRuleValueVO productRuleValueVO = schemeIdProductRuleValueMap.get(RuleCodeEnum.WORK_WECHAT.getCode());
//                if (WorkWechatSupportEnum.NO_SUPPORTED.getCode().equals(productRuleValueVO.getRuleValue())) {
//                    return ApiResult.error(BizRespStatusEnum.ORDER_OVER_LIMIT);
//                }
//            }
//        }
//        return ApiResult.success(Boolean.TRUE);
//    }
//
//    @Override
//    public ApiResult<WorkWechatStoreUserInfoVO> getStoreUserInfo(String code) {
//        ApiResult<String> tokenRes = SuiteAccessTokenManager.getToken(AppType.STORE);
//        if (!tokenRes.isSuccess()) {
//            log.warn("getUserInfoThird get suiteAccessToken fail");
//            return ApiResult.error(tokenRes);
//        }
//        Optional<UserInfoThirdVO> userInfoOp = WorkWechatSAO.getUserInfoThird(tokenRes.getResult(), code);
//        log.info("getUserInfoThird userInfoOp:{}", userInfoOp);
//        if (userInfoOp.isEmpty()) {
//            log.warn("The temporary authorization code is invalid, code = {}", code);
//            return ApiResult.error(BizCodeIntegrationEnum.TEMPORARY_CODE_INVALID);
//        }
//        WorkWechatStoreUserInfoVO workWechatUserInfoThirdVO = new WorkWechatStoreUserInfoVO();
//        CglibBeanCopyUtil.copyProperties(userInfoOp.get(), workWechatUserInfoThirdVO);
//        return ApiResult.success(workWechatUserInfoThirdVO);
//    }
//
//    @Override
//    public ApiResult<String> getCorpAccessToken(String corpId, String appId, String permanentCode) {
//        return CorpAccessTokenManager.getToken(corpId, appId, permanentCode);
//    }
//
//    @Override
//    public ApiResult<List<String>> getAppAdminList(String corpId, String agentId) {
//        ApiResult<String> tokenRes = SuiteAccessTokenManager.getToken(AppType.STORE);
//        if (!tokenRes.isSuccess()) {
//            log.warn("getAppAdminList get suiteAccessToken fail");
//            return ApiResult.error(tokenRes);
//        }
//        Optional<AdminListVO> adminListOp =
//                WorkWechatSAO.getAdminList(tokenRes.getResult(), GetAdminListDTO.builder().auth_corpid(corpId).agentid(agentId).build());
//        if (adminListOp.isEmpty()) {
//            log.warn("getAppAdminList fail, corpId = {}, agentId = {}", corpId, agentId);
//            return ApiResult.error(BizRespStatusEnum.IM_EXCEPTION);
//        }
//        return ApiResult.success(StreamX.safe(adminListOp.get().getAdmin()).map(AdminListVO.Admin::getUserId).toList());
//    }
//
//    @Override
//    public ApiResult<Boolean> sendWorthNotifyMsg(WorthMsgVO worthMsgVO) {
//
//        try {
//            log.debug("worth wechat start to send worth msg : worthMsgVO = {}", worthMsgVO);
//            //1.获取绑定的企业微信
//            ImOrgAppVO imOrgAppVO = notificationSender.getImOrgApp(worthMsgVO.getTenantId());
//            if (imOrgAppVO == null || imOrgAppVO.getImOrg() == null || imOrgAppVO.getImApp() == null){
//                log.warn("send user's worth week stat msg failed, im config not found! worthMsgId = {}",
//                        worthMsgVO.getId());
//                return ApiResult.success(Boolean.FALSE);
//            }
//            //2.获取用户绑定状态
//            Map<String, Long> userIdMap = notificationSender.getExternalUserIds(worthMsgVO.getTenantId(),
//                    worthMsgVO.getReceiverUserIdSet(), imOrgAppVO);
//
//           Collection<Long> notSendUserIdSet = MapUtil.isEmpty(userIdMap) ? worthMsgVO.getReceiverUserIdSet() :
//                   StreamX.safe(worthMsgVO.getReceiverUserIdSet()).filter(userId -> !userIdMap.values().contains(userId)).collect(Collectors.toSet());
//            MultiTenantUtil.execUnderTenantEnvV2(worthMsgVO.getTenantId(),  () ->
//                    worthSensorTracker.workWechatTrackNotifyNotSend(worthMsgVO.getWorthMsgBody().getMsgType(), notSendUserIdSet)
//            );
//            if (MapUtils.isEmpty(userIdMap)){
//                log.info("send user's worth week stat msg failed, not user need to send");
//                return ApiResult.success(Boolean.FALSE);
//            }
//
//            //3.生产消息内容
//            JSONObject jsonObject = specialMsgProducer.makeWorthNotifyMsg(imOrgAppVO.getImApp().getAppId(),
//                    userIdMap.keySet().stream().collect(Collectors.joining("|")), worthMsgVO);
//
//            //4.发送消息
//            Optional<MsgVO> msgVoOptional = WorkWechatSAO.sendMsg(AccessTokenManager.getTokenByType(imOrgAppVO), jsonObject);
//            if (!msgVoOptional.isPresent()){
//                log.warn("work wechat send worth msg failed：{}", worthMsgVO);
//                MultiTenantUtil.execUnderTenantEnvV2(worthMsgVO.getTenantId(),  () ->
//                        worthSensorTracker.workWechatTrackNotifyFailed(worthMsgVO.getWorthMsgBody().getMsgType(), userIdMap.values())
//                );
//                return ApiResult.success(Boolean.FALSE);
//            }
//            MultiTenantUtil.execUnderTenantEnvV2(worthMsgVO.getTenantId(),  () ->
//                    worthSensorTracker.workWechatTrackNotifySuccess(worthMsgVO.getWorthMsgBody().getMsgType(), userIdMap.values()));
//            log.info("work wechat send worth msg success:  worthMsgId = {}", worthMsgVO.getId());
//            return ApiResult.success(Boolean.TRUE);
//        } catch (Exception e){
//            log.warn("send user's worth week stat msg failed, worthMsgId = " + worthMsgVO.getId(), e);
//            MultiTenantUtil.execUnderTenantEnvV2(worthMsgVO.getTenantId(),  () ->
//                    worthSensorTracker.workWechatTrackNotifyFailed(worthMsgVO.getWorthMsgBody().getMsgType(), worthMsgVO.getReceiverUserIdSet())
//            );
//            return ApiResult.success(Boolean.FALSE);
//        }
//    }
//
//    @Override
//    public ApiResult<Boolean> sendLabFeatureNotifyMsg(LabFeatureMsgDTO dto) {
//        try {
//            log.debug("send lab feature notify msg start, dto = {}", dto);
//            ImOrgAppVO imOrgAppVO = notificationSender.getImOrgApp(dto.getTenantId());
//            if (imOrgAppVO == null || imOrgAppVO.getImOrg() == null || imOrgAppVO.getImApp() == null){
//                log.warn("send lab feature notify msg failed, im config not found! tenantId = {}", dto.getTenantId());
//                return ApiResult.success(Boolean.FALSE);
//            }
//            Map<String, Long> userIdMap = notificationSender.getExternalUserIds(dto.getTenantId(), dto.getReceiverUserIdSet(), imOrgAppVO);
//            if (MapUtils.isEmpty(userIdMap)){
//                log.info("send lab feature notify msg failed, not user need to send");
//                return ApiResult.success(Boolean.FALSE);
//            }
//            //生产消息内容
//            JSONObject jsonObject = specialMsgProducer.makeLabFeatureNotifyMsg(imOrgAppVO.getImApp().getAppId(),
//                    String.join("|", userIdMap.keySet()), dto);
//            //发送消息
//            Optional<MsgVO> msgVoOptional = WorkWechatSAO.sendMsg(AccessTokenManager.getTokenByType(imOrgAppVO), jsonObject);
//            if (msgVoOptional.isEmpty()){
//                log.warn("send lab feature notify msg failed, dto = {}", dto);
//                return ApiResult.success(Boolean.FALSE);
//            }
//            return ApiResult.success(Boolean.TRUE);
//        } catch (Exception e) {
//            log.error("send lab feature notify msg failed, dto = {}", dto, e);
//            return ApiResult.success(Boolean.FALSE);
//        }
//    }
//
//    @Override
//    public ApiResult<Boolean> unbindAllIntegrations() {
//        ImOrgDeleteDTO dto = new ImOrgDeleteDTO();
//        dto.setImType(ImType.WORK_WECHAT.getCode());
//        dto.setAppType(AppType.SELF.getCode());
//        integrationImService.delete(dto, false);
//        return ApiResult.success(true);
//    }
//}
