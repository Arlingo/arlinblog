from ragas import evaluate
from ragas.llms import LangchainLLMWrapper
from ragas.metrics import context_recall, context_precision, faithfulness, answer_relevancy

"""
        faithfulness: 忠实度（评估实际结果与上下文的一致性）
        context_precision: 上下文精度（使用上下文 + 问题，评估出与预期结果的相关性）
        context_recall: 上下文召回率（使用上下文 + 预期结果，评估出与实际结果的相关性）
        answer_relevancy: 答案相关性（评估实际结果与问题的一致性，不完整/包含冗余则扣分）
        """