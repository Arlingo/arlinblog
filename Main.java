import java.util.stream.Stream;

/**
 * @author: Arlin
 * @description: Main
 * @date: 2023/8/24 10:25
 * @copyright: 2023 智看科技 Inc. All rights reserved. 
 */
public class Main {

    public static void main(String[] args) {
        // 使用Stream.of方法创建一个包含字符串元素的流，元素包括："Twix", "Snickers", "Mars"
        Stream.of("Twix", "Snickers", "Mars")
                // 使用mapMulti方法对每个元素进行映射操作
                .mapMulti((item, consumer) -> {
                    // 调用consumer对象的accept方法，将元素item的大写形式传递给accept方法
                    consumer.accept(item.toUpperCase());
                    // 调用consumer对象的accept方法，将元素item的小写形式传递给accept方法
                    consumer.accept(item.toLowerCase());
                })
                // 使用forEach方法依次遍历流中的元素，并通过调用System.out::println方法将元素打印到控制台
                .forEach(System.out::println);

        /*
         String comment = "// 使用Stream.of方法创建一个包含字符串元素的流";
        */String comment = "使用flatMap 对每个元素进行转换，生成一个包含元素大写和小写的流";

        // flatMap 对每个元素进行转换，生成一个包含元素大写和小写的流
        Stream.of("Twix", "Snickers", "Mars")
                .flatMap(item -> Stream.of(item.toUpperCase(), item.toLowerCase()))
                .forEach(System.out::println);
    }

    /*
        public static void main(String[] args) {
        // 使用Stream.of方法创建一个包含字符串元素的流，元素包括："Twix", "Snickers", "Mars"
        Stream.of("Twix", "Snickers", "Mars")
                // 使用mapMulti方法对每个元素进行映射操作
                .mapMulti((item, consumer) -> {
                    // 调用consumer对象的accept方法，将元素item的大写形式传递给accept方法
                    consumer.accept(item.toUpperCase());
                    // 调用consumer对象的accept方法，将元素item的小写形式传递给accept方法
                    consumer.accept(item.toLowerCase());
                })
                // 使用forEach方法依次遍历流中的元素，并通过调用System.out::println方法将元素打印到控制台
                .forEach(System.out::println);

        // flatMap 对每个元素进行转换，生成一个包含元素大写和小写的流
        Stream.of("Twix", "Snickers", "Mars")
                .flatMap(item -> Stream.of(item.toUpperCase(), item.toLowerCase()))
                .forEach(System.out::println);
        }
        public static void main(String[] args) {
            // 使用Stream.of方法创建一个包含字符串元素的流，元素包括："Twix", "Snickers", "Mars"
            Stream.of("Twix", "Snickers", "Mars")
                    // 使用mapMulti方法对每个元素进行映射操作
                    .mapMulti((item, consumer) -> {
                        // 调用consumer对象的accept方法，将元素item的大写形式传递给accept方法
                        consumer.accept(item.toUpperCase());
                        // 调用consumer对象的accept方法，将元素item的小写形式传递给accept方法
                        consumer.accept(item.toLowerCase());
                    })
                    // 使用forEach方法依次遍历流中的元素，并通过调用System.out::println方法将元素打印到控制台
                    .forEach(System.out::println);

            // flatMap 对每个元素进行转换，生成一个包含元素大写和小写的流
            Stream.of("Twix", "Snickers", "Mars")
                    .flatMap(item -> Stream.of(item.toUpperCase(), item.toLowerCase()))
                    .forEach(System.out::println);
        }
        在注释中添加一行注释
        public static void main(String[] args) {}
        public static void main(String[] args) {
            // 使用Stream.of方法创建一个包含字符串元素的流，元素包括："Twix", "Snickers", "Mars"
            Stream.of("Twix", "Snickers", "Mars")
                    // 使用mapMulti方法对每个元素进行映射操作
                    .mapMulti((item, consumer) -> {
                        // 调用consumer对象的accept方法，将元素item的大写形式传递给accept方法
                        consumer.accept(item.toUpperCase());
                        // 调用consumer对象的accept方法，将元素item的小写形式传递给accept方法
                        consumer.accept(item.toLowerCase());
                    })
                    // 使用forEach方法依次遍历流中的元素，并通过调用System.out::println方法将元素打印到控制台
                    .forEach(System.out::println);

            // flatMap 对每个元素进行转换，生成一个包含元素大写和小写的流
            Stream.of("Twix", "Snickers", "Mars")
                    .flatMap(item -> Stream.of(item.toUpperCase(), item.toLowerCase()))
                    .forEach(System.out::println);
        }
        public static void main(String[] args) {
            // 使用Stream.of方法创建一个包含字符串元素的流，元素包括："Twix", "Snickers", "Mars"
            Stream.of("Twix", "Snickers", "Mars")
                    // 使用mapMulti方法对每个元素进行映射操作
                    .mapMulti((item, consumer) -> {
                        // 调用consumer对象的accept方法，将元素item的大写形式传递给accept方法
                        consumer.accept(item.toUpperCase());
                        // 调用consumer对象的accept方法，将元素item的小写形式传递给accept方法
                        consumer.accept(item.toLowerCase());
                    })
                    // 使用forEach方法依次遍历流中的元素，并通过调用System.out::println方法将元素打印到控制台
                    .forEach(System.out::println);

            // flatMap 对每个元素进行转换，生成一个包含元素大写和小写的流
            Stream.of("Twix", "Snickers", "Mars")
                    .flatMap(item -> Stream.of(item.toUpperCase(), item.toLowerCase()))
                    .forEach(System.out::println);
        }
     */

}
