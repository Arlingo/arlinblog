import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author: Arlin
 * @description: Main2
 * @date: 2024/1/9 16:46
 * @copyright: 2024 智看科技 Inc. All rights reserved.
 */
public class Main2 {

    private static class Data {
        public Long a;
        public Long b;

        public Data() {
        }

        public Data(Long a, Long b) {
            this.a = a;
            this.b = b;
        }

        public Long getA() {
            return a;
        }

        public void setA(Long a) {
            this.a = a;
        }

        public Long getB() {
            return b;
        }

        public void setB(Long b) {
            this.b = b;
        }
    }

    public static void main(String[] args) {
        List<Data> list = new ArrayList<>();
        list.add(new Data());
        long sum = list.stream().filter(Objects::nonNull).mapToLong(Data::getA).sum();
        System.out.println(sum);
    }
}
