
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author: Arlin
 * @date: 2024/7/9
 * @description:
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GitMergeRequestRowVO {

    private Long number;

    private Long bindId;

    private Long projectId;

    private String projectName;

    private String title;

    private String link;

    private String sourceBranch;

    private String targetBranch;

    private Integer commitCount;

    private Integer changedFiles;

    private Integer addLines;

    private Integer deleteLines;

    private Integer totalLines;

    private Integer commentCount;

    private Long gitUserId;

    private Long reviewerId;

    private Date createdAt;

    private Date mergedAt;

    private Boolean statisticsFlag;
}
